Netatmo Influxdb
======
Build Status:
------
[![pipeline status](https://gitlab.com/calebcall/netatmo-influxdb/badges/master/pipeline.svg)](https://gitlab.com/calebcall/netatmo-influxdb/commits/master)

Using this Image:
------
Running the container
A typical invocation of the container might be:

```
$ docker run -e INFLUXDB_HOST=127.0.0.1 \
      -e INFLUXDB_PORT=8086 \
      -e INFLUXDB_DATABSE=rainmachine \
      -e INFLUXDB_USERNAME=mrsprinkles \
      -e INFLUXDB_PASSWORD=supersecret \
      -e NETATMO_CLIENT_ID=lkjsdflkjhaskldfjn34298yusdf \
      -e NETATMO_CLIENT_SECRET=mysecretpassword \
      -e NETATMO_USERNAME=youremail@gmail.com \
      -e NETATMO_PASSWORD=sshhhitsasecret \
      -e SLEEP=30 \
      --name netatmo-influxdb \
      calebcall/netatmo-influxdb:latest
```

 > Netatmo Client ID and Secret can both be had by registering an app at:
 > https://dev.netatmo.com/en-US/myaccount/

The following environment variables can be used:
  - `INFLUXDB_HOST`: Set this to the host influxdb is running on (**default:** `localhost`)
  - `INFLUXDB_PORT`: Set this to the port your influxdb runs on (**default:** `8086`)
  - `INFLUXDB_DATABASE`: Set this to the database to write to (**default:** `rainmachine`)
  - `INFLUXDB_USERNAME`: Set this to the user to use to connect to influxdb with (**default:** `<blank>`)
  - `INFLUXDB_PASSWORD`: Set this to the password for the above user (**default:** `<blank>`)
  - `NETATMO_CLIENT_ID`: Client ID provided to you when registering your app with Netatmo (**default:** `<blank>`)
  - `NETATMO_CLIENT_SECRET`: Client Secret provided to you when registering your app with Netatmo (**default:** `<blank>`)
  - `NETATMO_USERNAME`: Username used to login to Netatmo (**default:** `<blank>`)
  - `NETATMO_PASSWORD`: Password used to login to Netatmo (**default:** `<blank>`)
  - `SLEEP`: Set this to the amount of time (in seconds) you want it to wait between polls (**default:** `300`)
  - `DEBUG`: Boolean used to create extra logging (**default:** `False`)

All of the above environment variables are *optional*, if not defined then it will use the **default** values.

Todo:
------
  - ~~Initial commit~~
  - TBD

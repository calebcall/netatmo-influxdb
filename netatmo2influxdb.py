#!/usr/bin/env python3
# encoding=utf-8

from pytz import timezone
import datetime
from influxdb import InfluxDBClient
import json
import netatmo
import os
import sys
from time import sleep as sleep
import logging

loglevel = os.getenv('LOGLEVEL', 'DEBUG')

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

#
debug_str=os.getenv("DEBUG", "False")
debug = debug_str.lower() == "true"

# settings from EnvionmentValue
netatmo_clientId=os.getenv('NETATMO_CLIENT_ID', "")
netatmo_clientSecret=os.getenv('NETATMO_CLIENT_SECRET', "")
netatmo_username=os.getenv('NETATMO_USERNAME')
netatmo_password=os.getenv('NETATMO_PASSWORD')
#
influxdb_host=os.getenv('INFLUXDB_HOST', "localhost")
influxdb_port=int(os.getenv('INFLUXDB_PORT', "8086"))
influxdb_username=os.getenv('INFLUXDB_USERNAME', "")
influxdb_password=os.getenv('INFLUXDB_PASSWORD', "")
influxdb_database=os.getenv('INFLUXDB_DATABASE', "Netatmo")
#
sleep_time = int(os.getenv('SLEEP', '300'))

# netatmo
authorization = netatmo.ClientAuth(clientId=netatmo_clientId,
                                clientSecret=netatmo_clientSecret,
                                username=netatmo_username,
                                password=netatmo_password)

devList = netatmo.WeatherStationData(authorization)
# influxdb
client = InfluxDBClient(host=influxdb_host,
                     port=influxdb_port,
                     username=influxdb_username,
                     password=influxdb_password)

# these keys are float
keylist=['Temperature', 'min_temp', 'max_temp', 'Pressure','AbsolutePressure']

def send_data(ds):
    #
    senddata={}
    dd=ds['dashboard_data']
    for key in dd:
        senddata["measurement"]=key
        #senddata["time"]=datetime.datetime.fromtimestamp(dd['time_utc']).strftime("%Y-%m-%dT%H:%M:%S")
        #if debug:
        #    logger.debug(senddata["time"])
        senddata["tags"]={}
        senddata["tags"]["host"]=ds['_id']
        if key in keylist:
            dd[key]=float(dd[key])
        senddata["fields"]={}
        senddata["fields"]["value"]=dd[key]
        if debug:
            logger.debug(json.dumps(senddata,indent=4))
        client.write_points([senddata], database=influxdb_database)

def sendDevList():
    for name in devList.modulesNamesList():
        if debug:
            logger.debug("--- module")
            logger.debug(name)
        ds=devList.moduleByName(name)
        if ds is None:
            continue
        if not 'dashboard_data' in ds:
            continue
        if debug:
            logger.debug(ds['_id'])
        send_data(ds)

    for station_id in devList.stations:
        if debug:
            logger.debug("--- station")
            logger.debug(station_id)
        ds=devList.stationById(station_id)
        if ds is None:
            continue
        if not 'dashboard_data' in ds:
            continue
        if debug:
            if 'station_name' in ds:
                logger.debug(ds['station_name'])
            else:
                logger.debug(station_id)
            logger.debug(ds['_id'])
        send_data(ds)
    logger.info("Data Sent")
    #sleep(sleep_time)

if __name__ == "__main__":
    # while True:
    sendDevList()

FROM alpine:latest

ENV PYTHONIOENCODING=utf-8

RUN apk add --update python3 &&\
    mkdir /netatmo &&\
    pip3 install influxdb &&\
    adduser -D netatmo

COPY netatmo2influxdb.py /netatmo/
COPY netatmo.py /netatmo/
COPY run.sh /netatmo/

RUN chmod +x /netatmo/run.sh && \
    chmod +x /netatmo/netatmo2influxdb.py

USER netatmo

CMD ["/netatmo/run.sh"]
